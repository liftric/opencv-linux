# opencv-linux

cf. https://github.com/Liftric/dockerbase-opencv

Usage:

```
cd opencv-build
docker build -t dockerbase-opencv .
docker run -it dockerbase-opencv bash
```

Then separate shell:

```
rm -rf include/ lib/ share/
docker cp `docker ps -q --filter ancestor=dockerbase-opencv`:/opencv-build/. ./
cd include/
ln -s opencv4/opencv2 opencv2
```
